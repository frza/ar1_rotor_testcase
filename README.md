# AR1: Analytical Rotor 1 Computational Fluid Dynamics Testcase

The aim with this rotor testcase is to provide a very simple rotor design,
for which the flow across the entire rotor is attached, with near constant
operational points (alpha, Cl, induction) along the span.

The rotor uses the NACA 63-618 airfoil along the entire rotor,
set to operate at Cl_d = 0.82 which at Re=10.e6 is at alpha_d = 2 deg,
with L/D = 147.7.

The rotor chord and twist is computed based on a basic analytical expression
without taking into account tip loss, see Døssing<sup>[1](#dossing)</sup>.
To ensure that the flow remains attached in the root region,
the blade twists to 90 degrees and blends to a symmetric airfoil,
joined to the two other blades with a 120 degree cut.

## CFD Surface and Volume Meshes

The surface mesh can be found in `grid/surface.xyz` in Plot3D format,
as well as `grid.x2d`, which is formatted specifically for the in-house
DTU developed EllipSys3D CFD code.

Alternatively, you can generate the surface grid yourself in Python,
and expert it to whichever format you prefer, using your own tools.
To generate the surface, firstly install PGL:

    $ git clone git@gitlab.windenergy.dtu.dk:frza/PGL.git
    $ cd PGL
    $ pip install -e .

Then clone the present repository:

    $ git clone git@gitlab.windenergy.dtu.dk:frza/ar1_rotor_testcase.git
    $ cd ar1_rotor_testcase/grid

and run the meshing script:

    $ python blademesher_ar1.py

This generates the above-mentioned mesh files ``surface.xyz`` that you can import in e.g.
Paraview or Fieldview for inspection, as well as the file ``grid.x2d``,
that is used by the volume mesh generator HypGrid3D.

The volume mesh provided is generated with the in-house mesh generator
HypGrid3D, and can be found in `grid/volumemesh.xyz` and `grid.x3dunf`,
Plot3D and EllipSys3D formats, respectively.

![chord](images/chord.png)
![twist](images/twist.png)
![rotor](images/rotor.png)
![root](images/root_surface_mesh_detail.png)
![flow](images/surfaceflow_w08_tran.png)

## References
<a name="dossing">1</a>:
Døssing, M 2011, Optimization of wind turbine rotors - using advanced aerodynamic and aeroelastic models and numerical optimization. Ph.D. thesis, Risø National Laboratory for Sustainable Energy, Technical University of Denmark, Roskilde. Risø-PhD, no. 69(EN)
