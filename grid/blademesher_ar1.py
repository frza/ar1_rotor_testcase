
import numpy as np

from PGL.components.blademesher import BladeMesher
from PGL.main.planform import read_blade_planform
from PGL.main.bezier import BezierCurve
from PGL.main.curve import Curve
from analytical_rotor import analytical_rotor

npt = 201
dist = np.array([[0., 1./npt, 1], [1., 1./npt/5., npt]])
from PGL.main.distfunc import distfunc
s = distfunc(dist)
#s = np.linspace(0, 1, npt)

chord, twist = analytical_rotor(s,
                                tsr=8.,
                                Cl_d=8.24e-01,
                                alpha_d=2.,
                                LD_d=153.7)
# fix nans in root
twist[0] = -90.
chord[0] = chord[1]

m = BladeMesher()

# planform entries
pf = {}
pf['s'] = s
pf['x'] = np.zeros(npt)
pf['y'] = np.zeros(npt)
pf['z'] = s
pf['rot_x'] = np.zeros(npt)
pf['rot_y'] = np.zeros(npt)
# insert analytical twist
pf['rot_z'] = twist
# insert analytical chord
pf['chord'] = chord
pf['rthick'] = np.ones(npt) * 0.18
pf['p_le'] = np.ones(npt) * 0.35
pf['dy'] = np.zeros(npt)

# path to the planform
#pf = read_blade_planform('data/DTU_10MW_RWT_blade_axis_prebend.dat')
#pf['rthick'] = np.minimum(pf['rthick'], 0.36)
m.pf = pf
# m.planform_filename = 'data/DTU_10MW_RWT_blade_axis_prebend.dat'

# spanwise and chordwise number of vertices
m.ni_span = 129
m.ni_chord = 257

# redistribute points chordwise
m.redistribute_flag = True
# number of points on blunt TE
m.chord_nte = 9
# set min TE thickness (which will open TE using AirfoilShape.open_trailing_edge)
# d.minTE = 0.0002

# user defined cell size at LE
# when left empty, ds will be determined based on
# LE curvature

dist = np.interp(np.linspace(0,1,20), [0, 0.11, 0.13, 1], [0.002, 0.002, 0.001, 0.0005])

m.dist_LE = np.array([np.linspace(0,1,20), dist]).T

from PGL.components.airfoil import AirfoilShape
af = AirfoilShape(points=np.loadtxt('naca63618.dat'))
# af = AirfoilShape(points=np.loadtxt('naca63018.dat'))
af.open_trailing_edge(0.005)

# airfoil family - can also be supplied directly as arrays
m.blend_var = [0.17, .19]
m.base_airfoils = [af.points, af.points]

# inputs to aero-root
m.root_type = 'cylinder'

# read root shape from file and only supply suction side
# of airfoil shape
m.root_con_shape = 'user_defined'
m.root_con = np.loadtxt('naca63018.dat')[100:, :]
# scale thickness to 48%
m.root_con[:, 1] *= 48./18.
# non-dimensional root chord
m.root_diameter = 0.15

m.ni_root = 15
m.s_root_start = 0.0
m.s_root_end = 0.125
m.ds_root_start = 0.012
m.ds_root_end = 0.01
m.root_con_smooth = 0.4
m.root_con_offset = -0.25
m.root_con_openTE = 0.075

# adjust these two to play with twist of the root Bezier curves
m.root_fW0 = .1
m.root_fW1 = .25

# add additional dist points to e.g. refine the root
# for placing VGs
# self.pf_spline.add_dist_point(s, ds, index)

# inputs to the tip component
# note that most of these don't need to be changed
m.ni_tip = 11
m.s_tip_start = 0.988
m.s_tip = 0.996
m.ds_tip_start = 0.002
m.ds_tip = 0.00025

m.tip_fLE1 = 1.  # Leading edge connector control in spanwise direction.
                 # pointy tip 0 <= fLE1 => 1 square tip.
m.tip_fLE2 = 1.  # Leading edge connector control in chordwise direction.
                 # pointy tip 0 <= fLE1 => 1 square tip.
m.tip_fTE1 = 1.  # Trailing edge connector control in spanwise direction.
                 # pointy tip 0 <= fLE1 => 1 square tip.
m.tip_fTE2 = 1.  # Trailing edge connector control in chordwise direction.
                 # pointy tip 0 <= fLE1 => 1 square tip.
m.tip_fM1 = 0.8   # Control of connector from mid-surface to tip.
                 # straight line 0 <= fM1 => orthogonal to starting point.
m.tip_fM2 = .8   # Control of connector from mid-surface to tip.
                 # straight line 0 <= fM2 => orthogonal to end point.
m.tip_fM3 = .8   # Controls thickness of tip.
                 # 'Zero thickness 0 <= fM3 => 1 same thick as tip airfoil.

m.tip_dist_cLE = 0.00025  # Cell size at tip leading edge starting point.
m.tip_dist_cTE = 0.00025  # Cell size at tip trailing edge starting point.
m.tip_dist_tip = 0.0008  # Cell size of LE and TE connectors at tip.
m.tip_dist_mid0 = 0.00025  # Cell size of mid chord connector start.
m.tip_dist_mid1 = 0.0001  # Cell size of mid chord connector at tip.

m.tip_c0_angle = 35.  # Angle of connector from mid chord to LE/TE

m.tip_nj_LE = 20   # Index along mid-airfoil connector used as starting point for tip connector


# generate the mesh
m.update()

# rotate domain with flow direction in the z+ direction and blade1 in y+ direction
m.domain.rotate_x(-90)
m.domain.rotate_y(180)

# copy blade 1 to blade 2 and 3 and rotate
m.domain.add_group('blade1', m.domain.blocks.keys())
m.domain.rotate_z(-120, groups=['blade1'], copy=True)
m.domain.rotate_z(120, groups=['blade1'], copy=True)


# split blocks to cubes of size 33^3
m.domain.split_blocks(33)

# Write EllipSys3D ready surface mesh
from PGL.main.domain import write_x2d
write_x2d(m.domain, scale_factor=100.)
# Write Plot3D surface mesh
# write_plot3d(m.domain, 'DTU_10MW_RWT_mesh.xyz')
m.domain.write_plot3d('surface.xyz')
