
import numpy as np

def analytical_rotor(r, tsr=6, Cl_d=0.8, alpha_d=4., LD_d=66., nB=3, design_CT=0.89):
    """
    """

    rad2deg = 180. / np.pi
    ni = 100

    # Cl_d = points

    CT = design_CT
    F = 0.988
    nB = nB
    CT_2d = CT / F
    tsr = tsr
    ltsr = r * tsr
    ld = LD_d
    Cl_d = Cl_d

    a = 0.08921 * (CT_2d / F)**3 + 0.05450 * (CT_2d / F)**2 + 0.25116 * (CT_2d / F)
    A1 = 0.5 * nB * (1 + tsr**2 / (1 - a)**2)**0.5
    A2 = CT_2d / tsr**2
    a_t = 0.5 * (1 + A2 / r**2)**0.5 - 0.5
    CT_F = -1.392 / (1.2 + A1) * CT_2d
    CT = CT_2d + CT_F

    CF_F = -1.4 / (2.3 + A1) * CT_2d
    CF = 2./3. * CT_2d + CF_F
    CP_d = -CF * tsr / ld
    CP_a = (4.906 * A2**2 - 1.173 * A2 - 0.002362) * CT_2d * (1. - a)
    CP = (1. - a) * CT + CP_d + CP_a

    # distributed
    gamma = CT_2d * np.pi / (nB * tsr) * F / (1 + a_t)
    Cp = CT_2d * F * ((1 -a) / (1 + a_t) - ltsr / ld)

    phi = np.arctan((1 - a) / (0.5 * (1 + (1 + A2 / r**2)) * tsr * r))
    twist = -(phi * rad2deg - alpha_d)
    chord = CT_2d * (2. * np.pi * F * r) / \
                (((1. - a)**2 + tsr**2 * r**2 * (1 + a_t)**2) * np.cos(phi)) / (Cl_d * nB)

    return chord, twist
