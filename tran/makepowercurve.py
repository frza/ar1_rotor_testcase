#!/usr/bin/env /home/frza/epd-7.2-2-rh5-x86_64/bin/python2.7
"""
power curve generation script for EllipSys3D.

Arguments
-----------
--wsp: list of <float>
    list of wind speeds

--oper : <string>
    operational data.
    file with columns <wsp> <pitch> <rot_speed> <rot_ampl>

--ti : <float> [None]
    turbulence intensity

Uses tag replacement of a template file for input.dat
and RunBatch1.1
Add tags with the same name as the input variable of the form:

winlet _winlet
rot_amplitude _rot_amplitude
pitch_setting _pitch_setting

etc.

Required files:
* input.dat <template>
* RunBatch1.1 (here you can also add a tagged name as e.g.
                #PBS -N DTURWT_winlet)
* flowfieldMPI
* grid.X3D
* grid.T3D

"""
import math
import sys
import os
import shutil
import commands
import numpy as np
import argparse
from subprocess import call

def replace_tags(inp,tags,file):
    for tag in tags:
        inp = inp.replace(tag,str(tags[tag]))
    lines = inp.splitlines()
    fid = open(file,'w')
    for line in lines:
        fid.write(line+'\n')
    fid.close()

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--wsp', dest='wsp', type=float,nargs='+',
                   help='list of wind speeds')
parser.add_argument('--oper', dest='operfile',type=str,
                   help='operational data file with columns:'
                        '<wsp> <pitch> <rot_speed> <rot_ampl>')
parser.add_argument('--ti', dest='ti',type=float, default=0.,
                   help='Turbulence intensity')
parser.add_argument('--omega', dest='omega',type=float, default=1.e3,
                   help='Omega')
parser.add_argument('--dist', dest='dist',type=float, default=1.e3,
                   help='Omega')
parser.add_argument('--dry-run', dest='dryrun',type=bool, default=False,
                   help='test the setup without submitting job')

args = parser.parse_args()
wsp = args.wsp
operfile = args.operfile
oper = np.loadtxt(operfile)
#ti = args.ti
#omega = args.omega
#dist = args.dist

from ellipsys_input import read_ellipsys_input
inp = read_ellipsys_input('input.dat') 
fiid = open('RunBatch1.1','r')
runb = fiid.read()

for w in wsp:
    dir = 'w'+str('%2.2f' % w)
    try:
        os.mkdir(dir)
    except:
        print 'failed creating directory'
        print 'it probably exists, appending data ...'

    # ktmp = 0.
    # if ti > 0.:
    #     k=(ti*w/100.)**2*1.5e0
    #     ktmp = k*(1+omega*.09e0*dist/w)**.92e0

    pitch_setting = np.interp(w,oper[:,0],oper[:,1])
    omega = np.interp(w,oper[:,0],oper[:,2]) * np.pi * 2. / 60.
    # pitch_setting = 0.
    path = dir + '/input.dat'
    inp.set_entry('winlet', w)
    inp.set_entry('wfarfield', w)
    inp.set_entry('pitch_setting', pitch_setting)
    inp.set_entry('rot_amplitude', omega)
    inp.write(path)
    path = dir + '/RunBatch1.1'
    tags = dict(_winlet=w)
    replace_tags(runb,tags,path)
    # shutil.copy('flowfieldMPI',dir)
    os.chdir(dir)
    # pitch(pitch_setting)
    command = 'ln -s ../grid.*3D .'
    cmd = commands.getoutput(command)
#   cmd = commands.getoutput('rm grid.rst')
#   cmd = commands.getoutput('ln -s grid.RST.01 grid.rst')
    if not args.dryrun:
        cmd = commands.getoutput('qsub RunBatch1.1')
    print cmd
    os.chdir('..')



