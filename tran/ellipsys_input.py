
import numpy as np


def _convert_val(val):
    """
    convert a string value read from a file into either int, float or
    string
    """

    try:
        return int(val)
    except:
        try:
            return float(val)
        except:
            return val


class Entry(object):
    """class for "name: val" entries in input files"""

    def __init__(self, name, val, comment=False):

        self.name = name
        self.val = val
        self.comment = comment

    def _print(self, tab=0):
        """pretty print the entry"""

        if self.comment:
            return '%s\n' % (self.val)

        if isinstance(self.val, str):
            return '%s%s\n' % (tab * ' ', self.name + ' ' + self.val)
        try:
            return '%s%s\n' % (tab * ' ', self.name + ' ' +
                             ' '.join(map(str, self.val)))
        except:
            return '%s%s\n' % (tab * ' ', self.name + ' ' +
                             str(self.val))


class EllipSysInput(object):
    """Class for list of Entry objects in an EllipSys input file"""

    def __init__(self, name):

        self.name = name
        self.entries = []
        self.comment = ''

    def get_entry(self, name, obj=False):
        """convenience method to get a specific entry from section"""

        items = []
        for entry in self.entries:
            if entry.name == name:
                if obj:
                    items.append(entry)
                else:
                    if isinstance(entry.val, list):
                        items.append(entry.val)
                    else:
                        items.append([entry.val])

        if len(items) == 0:
            return None
        if len(items) == 1:
            return items[0]
        else:
            return items

    def set_entry(self, name, val, index=0):

        entries = self.get_entry(name, obj=True)
        if entries == None or entries == []:
            self.entries.append(Entry(name, val))
        else:
            if isinstance(entries, list):
                entries[index].val = val
            else:
                entries.val = val

    def _next(self):

        try:
            entry = self.entries[self.pos]
            self.pos += 1
            return entry
        except:
            return False

    def _print(self, tab=0):
        """pretty print section recursively"""

        self.pos = 0
        string = ''

        while self.pos < len(self.entries):
            entry = self._next()
            string += entry._print(tab=tab)

        return string

    def write(self, name):

        fid = open(name, 'w')
        fid.write(self._print())
        fid.close()


def read_ellipsys_input(filename):
    """ reads an EllipSys input file """

    # initialize output dictionary
    input_dict = EllipSysInput('main')

    input_file = open(filename)

    # process each line
    while 1:
        # read the line
        line = input_file.readline()
        if not line:
            break

        # remove line returns
        line = line.strip('\r\n')
        # make sure it has useful data
        if line == '':
            continue
        if line[0] == '#':
            input_dict.entries.append(Entry('comment', line, comment=True))
            continue
        # split across equals sign
        line = line.split()
        param = line[0].strip()
        if param == 'stop':
            break
        vals = [_convert_val(val) for val in line[1:]]
        if len(vals) == 1:
            input_dict.entries.append(Entry(param, vals[0]))
        else:
            input_dict.entries.append(Entry(param, vals))

    return input_dict
